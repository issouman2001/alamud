from .event import Event3,Event2
class ShootWith(Event3):
    NAME = "shoot-on"

    def perform(self):
        self.inform("shoot-on")