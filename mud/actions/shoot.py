from .action import Action2, Action3
from mud.events import ShootWithEvent

class ShootWithAction(Action3):
    EVENT = ShootWithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "shoot-on"
